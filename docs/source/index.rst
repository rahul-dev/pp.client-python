.. Produce & Publish Python Client documentation master file, created by
   sphinx-quickstart on Sat Jul  6 08:40:52 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Produce & Publish Python Client
===============================

.. toctree::
   :maxdepth: 2

   README.rst
   CHANGES.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

